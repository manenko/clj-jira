(ns build
  (:require [clojure.tools.build.api :as b]
            [deps-deploy.deps-deploy :as d]))

(def lib       'manenko/clj-jira)
(def version   "0.1.15")
(def class-dir "target/classes")
(def basis     (b/create-basis {:project "deps.edn"}))
(def jar-file  (format "target/%s-%s.jar" (name lib) version))
(def scm       {:url "https://gitlab.com/manenko/clj-jira"
                :connection "scm:git:git://git@gitlab.com:manenko/clj-jira.git"
                :developerConnection "scm:git:git://git@gitlab.com:manenko/clj-jira.git"
                :tag (format "v%s" version)})

(defn clean [_]
  (b/delete {:path "target"}))

(defn jar [_]
  (b/write-pom {:class-dir  class-dir
                :lib        lib
                :scm        scm
                :version    version
                :basis      basis
                :src-dirs   ["src"]})
  (b/copy-dir  {:src-dirs   ["src" "resources"]
                :target-dir class-dir})
  (b/jar       {:class-dir  class-dir
                :jar-file   jar-file}))

(defn deploy [_]
  (jar _)
  (d/deploy {:pom-file       (format "%s/META-INF/maven/manenko/clj-jira/pom.xml" class-dir)
             :artifact       jar-file
             :installer      :remote
             :sign-releases? false}))

(ns manenko.clj-jira.request.core
  (:require
   [manenko.clj-jira.request.util :as util]
   [manenko.clj-jira.middleware   :as middleware]))


;; User
;; -----------------------------------------------------------------------------

(defn user-current-get
  "Returns a ring-style request map to get information about a current user.

  **Arguments:**

  opts
  : An optional map that configures a Jira query and supports the following
    keys:
    * `:expand` - A sequence of fields to include to the response.  Supports the
      following values:
        - `:group` - returns all groups, including nested groups, the user
          belongs to.
        - `:applicationRoles` - returns the application roles the user is
          assigned to.

  The function creates a request that uses [Get current user] Atlassian Jira API.

  [Get current user]: https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-myself-get"
  ([]
   (user-current-get {}))
  ([opts]
   {::middleware/api-resource "/myself"
    :method                   :get
    :query-params             (util/opts->query-params
                                opts
                                {:expand util/make-comma-separated-string})}))


(defn user-search
  "Returns a ring-style request map to search Jira users.

  **Arguments:**

  opts
  : A map that configures a Jira query and supports the following
    keys:
  
    * `:query` - A query string that is matched against user display name and
      email address to find relevant users.  For example, `{:query \"john\"}`
      matches a user with a display name of `\"John Smith\"` and a user with
      an email address of `\"johnson@example.com\"`.  Required, unless
      `:accountId` is provided.
    * `:accountId`  - A query string that is matched exactly against a user
      account ID.  Required, unless `:query` is provided.
    * `:startAt`    - A page offset.  `Integer`.  Default is `0`.
    * `:maxResults` - A page size.  `Integer`.  Default is `50`.
      Maximum is `1000`.

  The function creates a request that uses [Find users] Atlassian Jira API.

  [Find users]: https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-user-search-get"
  [opts]
  {::middleware/api-resource "/user/search"
   :method                   :get
   :query-params             (util/opts->query-params
                               opts
                               {:query      identity
                                :accountId  identity
                                :startAt    identity
                                :maxResults identity})})


;; Project
;; -----------------------------------------------------------------------------

(defn project-search
  "Returns a ring-style request map to get projects visible to a current user.

  **Arguments:**

  opts
  : An optional map that configures a Jira query and supports the following
    keys:
    * `:startAt` - A page offset.  Integer.  Default is `0`.
    * `:maxResults` - A page size.  Integer.  Default is `50`.  Maximum is `50`.
    * `:orderBy` - Orders results by a field.  The following values are
      supported:
        - `:category`
        - `:key`
        - `:name`
        - `:owner`
        - `:issueCount`
        - `:lastIssueUpdatedTime`
  
      Default is `:key`, which sorts projects alphabetically by project key.
      You can prepend a value with `+` or `-` to specify a sort direction, e.g.
      `:+owner`, `:-category`, etc.
    * `:query` - Searches for projects which `key` or `name` matches the given
       string.
    * `:typeKey` - Orders results by the project type.  A sequence of the
      following values:
        - `:business`
        - `:service_desk`
        - `:software`
    * `:categoryId` - An identifier of the project's category.  Integer.
    * `:searchBy` - Default is `[:key, :name]`.
    * `:action` - Filters results by projects for which user can view, browse,
      or edit the project.  Supports the following values:
        - `:view`
        - `:browse`
        - `:edit`
      Default is `view`.
    * `:expand` - A sequence of fields to include to the response.  Supports the
      following values:
        - `:description`
        - `:projectKeys`
        - `:lead`
        - `:issueTypes`
        - `:url`

  The function creates a request that uses [Get projects paginated] Atlassian Jira API.

  [Get projects paginated]: https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-project-search-get"
  ([opts]
   {::middleware/api-resource "/project/search"
    :method                   :get
    :query-params             (util/opts->query-params
                                opts
                                {:action     identity
                                 :categoryId identity
                                 :expand     util/make-comma-separated-string
                                 :maxResults identity
                                 :orderBy    util/make-comma-separated-string
                                 :query      identity
                                 :searchBy   util/make-comma-separated-string
                                 :startAt    identity
                                 :typeKey    identity})})
  ([]
   (project-search {})))


(defn project-get
  "Returns a ring-style request map to get a project by its ID/key.

  **Arguments:**

  id-or-key
  : Project ID or key (case sensitive).

  opts
  : An optional map that configures a Jira query and supports the following
    keys:
    * `:expand` - A sequence of fields to include to the response.  Supports the
      following values:
        - `:description`
        - `:projectKeys`
        - `:lead`
        - `:issueTypes`
        - `:issueTypeHierarchy`
    * `:properties` - A sequence of project properties to return for the project.

  The function creates a request that uses [Get project] Atlassian Jira API.

  [Get project]: https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-project-projectIdOrKey-get"
  ([id-or-key opts]
   {::middleware/api-resource (str "/project/" id-or-key)
    :method                   :get
    :query-params             (util/opts->query-params
                                opts
                                {:expand     util/make-comma-separated-string
                                 :properties util/make-comma-separated-string})})
  ([id-or-key]
   (project-get id-or-key {})))


;; Issue
;; -----------------------------------------------------------------------------

(defn issue-get
  "Returns a ring-style request map to get an issue by its ID/key.

  **Arguments:**

  id-or-key
  : Issue ID or key.

  opts
  : An optional map that configures a Jira query and supports the following
    keys:
    * `:fields` - A sequence of fields to return for the issue.  Use it to
      retrieve a subset of fields.  Allowed values:
        - `:*all`       returns all fields.
        - `:*navigable` returns navigable fields.
        - any issue field, prefixed with a minus to exclude.
  
      For example:
  
        - `[:summary :comment]` returns only `summary` and `comment` fields.
        - `[:-description]` returns all default fields except `description`.
        - `[:*navigable :-comment]` returns all navigable fields except `comment`.
    * `:fieldsByKeys` - Indicates whether fields in `fields` are referenced by
      keys rather than IDs.  `Boolean`.  Default is `false`.
    * `:expand` - A sequence of fields to include to the response.  Supports the
      following values:
        - `:renderedFields` - Returns field values rendered in HTML format.
        - `:names` - Returns the display name of each field.
        - `:schema` - Returns the schema describing a field type.
        - `:transitions` - Returns all possible transitions for the issue.
        - `:editmeta` - Returns information about how each field can be edited.
        - `:changelog` - Returns a list of recent updates to an issue, sorted by
          date, starting from the most recent.
        - `:versionedRepresentations` - Returns a sequence for each version of a
          field's value, with the highest number representing the most recent
          version. Note: When included in the request, the `:fields` parameter
          is ignored.
    * `:properties` - A sequence of issue properties to return.  Allowed values:
        - `[:*all]` - Returns all issue properties.
        - `[:*all :-prop1]` - Returns all properties, except `prop1`.
        - `[:prop1 :prop2]` - Returns `prop1` and `prop2` properties.
    * `:updateHistory` - Whether the project in which the issue is created is
      added to the user's **Recently viewed** project list, as shown under
      **Projects** in Jira.  `Boolean`.  Default is `false`.

  All fields are returned by default.

  The function creates a request that uses [Get issue] Atlassian Jira API.

  [Get issue]: https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-issue-issueIdOrKey-get"
  ([id-or-key opts]
   {::middleware/api-resource (str "/issue/" id-or-key)
    :method                   :get
    :query-params             (util/opts->query-params
                               opts
                               {:fields util/make-comma-separated-string})})
  ([id-or-key]
   (issue-get id-or-key {})))


(defn issue-search
  "https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-search-post"
  ([jql opts]
   {::middleware/api-resource "/search"
    :method                   :post
    :body                     (-> opts
                                  (util/opts->body-params
                                   {:startAt       identity
                                    :maxResults    identity
                                    :fields        util/make-comma-separated-string
                                    :validateQuery identity
                                    :expand        identity ;; TODO: check if it works OOB
                                    :properties    util/make-comma-separated-string
                                    :fieldsByKeys  identity})
                                  (assoc :jql jql))})
  ([jql]
   (issue-search jql {})))



;; Filter
;; -----------------------------------------------------------------------------


;; JQL
;; -----------------------------------------------------------------------------



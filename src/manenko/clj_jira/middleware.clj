(ns manenko.clj-jira.middleware
  "Ring-style middleware for Jira REST API requests.")


;; Middleware
;; -----------------------------------------------------------------------------

(defn ^:private create-api-url
  "Returns a full Jira REST API URL of the given `resource` on the
  given `host`."
  [host resource]
  (str "https://" host "/rest/api/3" resource))


;; Request headers
;; -----------------------------------------------------------------------------
;; - `x-atlassian-force-account-id=true``.  Operations will behave as if GDPR
;;                                          changes are enforced (for example,
;;                                          deprecated fields removed).  Use
;;                                          this header to test if your
;;                                          integration is GDPR-compliant.
;;

;; Response headers
;; -----------------------------------------------------------------------------
;; - `X-AACCOUNTID=id`.  This response header contains the Atlassian account ID
;;                       of the authenticated user.

(defn wrap-api
  "Creates a [Ring-style] middleware that configures a request map
  to make it work with Jira REST API.

  **Arguments**

  **`host`**

  Hostname of the Jira instance to connect to.  It can be `nil`.  In this case
  the function will look for `::jira-host` key in the request map.

  [Ring-style]: https://github.com/ring-clojure/ring/blob/master/SPEC"
  ([]
   (wrap-api nil))
  ([host]
   (fn [handler]
     (letfn [(resolve-url [request]
               (create-api-url (or host (::jira-host request))
                               (::api-resource request)))
             (add-api-headers [request]
               (-> request
                   (assoc :url (resolve-url request))
                   (assoc :x-atlassian-force-account-id true)
                   (assoc-in [:headers "Content-Type"] "application/json")
                   (dissoc ::api-resource ::jira-host)))]
       (fn
         ([request]
          (handler (add-api-headers request)))
         ([request respond raise]
          (handler (add-api-headers request) respond raise)))))))


(defn wrap-token-auth
  "Creates a [Ring-style] middleware that configures a request map
  to use [Basic auth for REST APIs].

  **Arguments**

  **`email`**

  Atlassian account email address.  Can be `nil`.

  **`token`**

  Atlassian [API token].  Can be `nil`.

  If either of `email` or `token` is `nil`, the function will look for
  `::jira-email` and `::jira-token` keys respectively in the request map and use
  their values instead.

  The middleware adds `:basic-auth` key to the request map and you
  have to use it to setup basic HTTP auth headers somewhere later by
  yourself or using corresponding middleware from HTTP client library
  you use.  For example, for [clj-http] you can do this:

  ```clojure
  (ns manenko.clj-jira.example
    (:require [clj-http.client             :as client]
              [manenko.clj-jira.core       :as jira]
              [manenko.clj-jira.middleware :as middleware]))

  ,,,

  (client/with-middleware
    (conj 
     client/default-middleware
     (middleware/wrap-api        host)
     (middleware/wrap-token-auth email token))
    (client/request m))
  ```

  The `clj-http.client/default-middleware` var includes
  `clj-http.client/wrap-basic-auth` middleware which looks for
  `:basic-auth` key in a request map and configures the request for
  basic HTTP authentication.

  [Basic auth for REST APIs]: https://developer.atlassian.com/cloud/jira/platform/jira-rest-api-basic-authentication/
  [API token]:  https://confluence.atlassian.com/cloud/api-tokens-938839638.html
  [Ring-style]: https://github.com/ring-clojure/ring/blob/master/SPEC
  [clj-http]:   https://github.com/dakrone/clj-http"
  ([]
   (wrap-token-auth nil nil))
  ([email token]
   (fn [handler]
     (letfn [(add-auth-headers [request]
               (-> request
                   (assoc
                     :basic-auth
                     [(or email (::jira-email request))
                      (or token (::jira-token request))])
                   (dissoc ::jira-email ::jira-token)))]
       (fn
         ([request]
          (handler (add-auth-headers request)))
         ([request respond raise]
          (handler (add-auth-headers request) respond raise)))))))

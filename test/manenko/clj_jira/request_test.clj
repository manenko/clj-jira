(ns manenko.clj-jira.request-test
  (:require
    [clojure.test                  :as test]
    [manenko.clj-jira.request.core :as request]
    [manenko.clj-jira.middleware   :as middleware]))


(test/deftest test-user-current
  (test/testing "user-current without parameters should not provide any
  query parameters"
    (test/is (= (request/user-current-get)
                {::middleware/api-resource "/myself"
                 :method                   :get
                 :query-params             {}})))

  (test/testing "user-current with a single field field to expand should add
  that field to query parameters map"
    (test/is (= (request/user-current-get {:expand [:group]})
                {::middleware/api-resource "/myself"
                 :method                   :get
                 :query-params             {:expand "group"}})))

  (test/testing "user-current with multiple fields to expand should add a
  comma-separated string that consist of names of those fields to the query
  parameters map"
    (test/is (= (request/user-current-get {:expand [:group :applicationRoles]})
                {::middleware/api-resource "/myself"
                 :method                   :get
                 :query-params             {:expand "group,applicationRoles"}})))

  (test/testing "user-current should ignore options it does not understand"
    (test/is (= (request/user-current-get {:expand [:group :applicationRoles]
                                       :dha    :werda})
                {::middleware/api-resource "/myself"
                 :method                   :get
                 :query-params             {:expand "group,applicationRoles"}}))))


(test/deftest test-user-search
  (test/is (= (request/user-search {:query      "dha-werda"
                                    :maxResults 100
                                    :unused     :nothing})
              {::middleware/api-resource "/user/search"
               :method                   :get
               :query-params             {:query      "dha-werda"
                                          :maxResults 100}})))


(test/deftest test-project-search
  (test/testing "project-search without parameters should not add any additional
  request parameters"
    (test/is (= (request/project-search)
                {::middleware/api-resource "/project/search"
                 :method                   :get
                 :query-params             {}})))

  (test/testing "project-search should ignore options it does not understand"
    (test/is (= (request/project-search {:query      "dha"
                                         :u-n-u-s-ed "werda"})
                {::middleware/api-resource "/project/search"
                 :method                   :get
                 :query-params             {:query "dha"}})))

  (test/testing "project-search should correctly convert its options into query
  parameters"
    (test/is
      (= (request/project-search {:query   "dha-werda"
                                  :startAt 14
                                  :orderBy [:issueCount :-category :+owner]
                                  :expand  [:url]})
         {::middleware/api-resource "/project/search"
          :method                   :get
          :query-params             {:query   "dha-werda"
                                     :startAt 14
                                     :orderBy "issueCount,-category,+owner"
                                     :expand  "url"}}))))

(ns manenko.clj-jira.middleware-test
  (:require
    [clojure.test                :as test]
    [manenko.clj-jira.middleware :as sut]))


(test/deftest test-wrap-token-auth-middleware
  (test/testing "A middleware without parameters should look for email and token
  in a request map"
    (let [middleware (sut/wrap-token-auth)]
      (test/is (= ((middleware identity)
                   {::sut/jira-email "dha@werda.org"
                    ::sut/jira-token "wINKABMudNv31jeKfFE13DAE"})
                  {:basic-auth ["dha@werda.org" "wINKABMudNv31jeKfFE13DAE"]}))))

  (test/testing "A middleware with both parameters set to nil should look for
  email and token in a request map"
    (let [middleware (sut/wrap-token-auth nil nil)]
      (test/is (= ((middleware identity)
                   {::sut/jira-email "dha@werda.org"
                    ::sut/jira-token "wINKABMudNv31jeKfFE13DAE"})
                  {:basic-auth ["dha@werda.org" "wINKABMudNv31jeKfFE13DAE"]}))))

  (test/testing "A middleware with email set to nil should look for it inside a
  request map"
    (let [middleware (sut/wrap-token-auth nil "wINKABMudNv31jeKfFE13DAE")]
      (test/is (= ((middleware identity)
                   {::sut/jira-email "dha@werda.org"
                    ::sut/jira-token "AAAAAAAAAAAAAAAAAAAA"})
                  {:basic-auth ["dha@werda.org" "wINKABMudNv31jeKfFE13DAE"]}))))

  (test/testing "A middleware with token set to nil should look for it inside a
  request map"
    (let [middleware (sut/wrap-token-auth "dha@werda.org" nil)]
      (test/is (= ((middleware identity)
                   {::sut/jira-email "aaa@bbbb.org"
                    ::sut/jira-token "wINKABMudNv31jeKfFE13DAE"})
                  {:basic-auth ["dha@werda.org" "wINKABMudNv31jeKfFE13DAE"]}))))

  (test/testing "A middleware with both parameters set to non-nil should use
  them"
    (let [middleware (sut/wrap-token-auth "dha@werda.org"
                                          "wINKABMudNv31jeKfFE13DAE")]
      (test/is (= ((middleware identity)
                   {::sut/jira-email "aaaa@bbbb.com"
                    ::sut/jira-token "abcdefghijklmnopqrstuvwxyz"})
                  {:basic-auth ["dha@werda.org" "wINKABMudNv31jeKfFE13DAE"]})))))
